---
layout: handbook-page-toc
title: "Gainsight Dashboards"
description: "An overview of the logic going into the reports found within the Gainsight Dashboards."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## TAM Proactive Dashboard



### Upcoming this Month



***Cadence Calls Due:** 

Count of P1 customers that haven’t had a Timeline Entry with Meeting type: Cadence Call in the past 30 days and P2 Customers in the past 60 days



***Upcoming CTAs:**

Count of all New/Work in Progress CTAs that are due in the next 30 days



***Upcoming Success Plan Tasks:**

​		Count of open success plan tasks due in the next 30 days. Does not include any 

​		overdue CTAs



***Upcoming EBRs for Scheduling:**

​		Count of active EBRs that are due in the next 30 days. Does not look at overdue CTAs  



​		

​

### ​Upcoming This Quarter



***Upcoming Success Plan Objectives:**

​		Open/WIP Objectives due this fiscal year, not including any that are past their due date.



***Upcoming Stage Expansion:**

Count of open/WIP expansion stage adoption objectives due over the current quarter



***Upcoming Stage Enablement:**

Count of open/WIP enablement stage adoption objectives due over the current quarter



***Upcoming Renewals**

Count of renewal opportunities with ARR > 50000 that aren’t closed or unqualified with a close date in the current quarter



***Upcoming Upsell Due to Close**

Count of “add-on business” opportunities that aren’t closed but have a close date in the current quarter



​		

​		
### Health and Utilization
​		

​***Poor License Utilization**

Count of customers whose License Utilization is Red. This means their billable user count/licenses sold is :

<10% and the customer is at least 2 months old

<50% and the customer’s more than 6 months old

<75% and the customer’s more than 9 months old



***High License Utilization**

Count of customers whose License Utilization exceeds 90%



***Adopted SCM But Not CI:**

Count of customers whose SCM health measure is Green and CI is Red

Merge Requests utilization >= 25 and SMAU Verify Utilization < 20



***DevSecOps Poor Health**

Count of customers whose DevSecOps health measure is red. This means their SMAU Secure Utilization < 15
